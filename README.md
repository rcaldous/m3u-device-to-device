# M3U-Device-To-Device

Takes in .m3u playlist file and reconstructs the same playlist on a different device

If the local music folder and the device have the same directory structure you can just do a replace all in a text editor but this will find the files wherever they are and reconstruct it.

## Using
```
java -jar <jarfile.jar> "playlist file.m3u" folder/device
```
arg[0] = playlist files  
arg[1] = parent directory of device where new playlist is to be created


## Planned features:
- Ability to select a separate playlists folder, at the moment they're dumped in the device root directory
- MD5 lookup (wont have to rely on files being named the same across devices)
- ID3 lookup (same song, different MD5/filename)
- docker version
