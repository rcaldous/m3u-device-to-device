package com.richardaldous.m3umapper;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class M3uMapperTest {

  @BeforeAll
  static void createPlaylist() {
    String[] args = {"folder/home/beep.m3u","folder/device/"};
    M3uGenerator.main(args);
  }
  
  @Test
  void findSongDevice() {
    M3uGenerator.filesToMap("folder/device");
    Map<String, File> files = M3uGenerator.getFiles();
    assertTrue(files.containsKey("beep.wav"));
    assertTrue(files.containsKey("beow.wav"));
  }

  @Test
  void readM3U() {
    File m3u = new File("folder/home/beep.m3u");
    Playlist play = new Playlist(m3u);
    Map<String,File> music = play.getMusicMap();
    assertTrue(music.containsKey("beep.wav"));
  }

  @Test
  void readGeneratedPlaylist() {
    File m3u = new File("folder/device/beep-gen.m3u");
    Playlist play = new Playlist(m3u);
    Map<String,File> music = play.getMusicMap();
    assertTrue(music.containsKey("beep.wav"));
  }
  
  @Test
  void playlistCreated() {
    assertTrue(new File("folder/device/beep-gen.m3u").exists());
  }



}
