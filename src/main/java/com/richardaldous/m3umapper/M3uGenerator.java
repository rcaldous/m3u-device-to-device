package com.richardaldous.m3umapper;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class M3uGenerator {
  private static Logger log = Logger.getGlobal();
  private static Map<String, File> files = new HashMap<>();
  
  public static Map<String, File> getFiles() {
    return files;
  }

  // args[0] = playlist file
  // args[1] = device folder
  // args[2] = playlist folder on device
  
  public static void main(String[] args) {
    if (args.length != 2) {
      return;
    }
    log.info("Playlist: " + args[0] + '\n' + "Device: " + args[1]);
    filesToMap(args[1]);
    log.info("Found " + files.size() + " files");
    Playlist play = new Playlist(new File(args[0]));
    Map<String, File> music = play.getMusicMap();
    log.info("Playlist contains " + music.size() + " entries");
    Playlist deviceList = new Playlist(new File(args[0]).getName(),args[1]);
    for (String key : music.keySet()) {
      if (files.containsKey(key)) {
        deviceList.addFile(files.get(key));
      }
    }
    deviceList.print();
  }

  public static void filesToMap(String parent) {
    File file = new File(parent);
    String[] folders = file.list();
    for (String folder: folders) {
      folder = parent + File.separatorChar + folder;
      File fold = new File(folder);
      if (fold.isDirectory()) {
        filesToMap(fold.getAbsolutePath()); 
      }
      else if (fold.isFile()) {
        files.put(fold.getName(), fold);
      }
    }
  }


}
