package com.richardaldous.m3umapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

class Playlist {
  Map<String, File> music = new HashMap<>();
  String title;
  String parent;
  private Logger log = Logger.getGlobal();
  
  public Map<String, File> getMusicMap() {return music;}
  
  public Playlist(File f) {
    try (BufferedReader br = new BufferedReader(new FileReader(f))) {
      String line;
      while ((line = br.readLine()) != null) {
         if (line.charAt(0) != '#') {
           File lineFile;
           if (File.separatorChar == '/') {
             lineFile = new File(line.replace('\\', '/'));
           } else {
             lineFile = new File(line.replace('/', '\\'));
           }
           addFile(lineFile);
         }
      }
    } catch (IOException e) {
      log.warning(e.getMessage());
    }
  }

  public Playlist(String title, String parent) {
    if (title.endsWith(".m3u")) {
      this.title = title.substring(0,title.length()-4);
      this.title = this.title + "-gen.m3u";
    } else {
      this.title = title + "-gen.m3u";
    }
    this.parent = parent;
  }

  public void addFile(File f) {
    music.put(f.getName(), f);
  }

  public void print(String folder) {
    parent = parent + File.separatorChar + folder;
    print();
  }

  private String getRelative(File f) {
    // https://stackoverflow.com/a/205655/7768076
    return
      new File(parent)
      .toURI()
      .relativize(new File(f.getAbsolutePath())
      .toURI())
      .getPath();
  }

  public void print() {
    StringBuilder stb = new StringBuilder();
    if (music.isEmpty()) {
      return;
    }
    try (FileWriter fw = new FileWriter(parent + File.separatorChar + title)) {
      for (Map.Entry<String, File> entry : music.entrySet()) {
        stb.append(getRelative(entry.getValue()));
      }
      fw.write(stb.toString());
    } catch (IOException e) {
      log.warning(e.getMessage());
    }
  }
}
